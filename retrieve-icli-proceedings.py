import gensim
import xml.etree.ElementTree as ET
import requests
import re
import os
from urllib.request import urlretrieve

work_pwd = os.getcwd()

data_pwd = work_pwd + '/data'

tree = ET.parse('icli.xml')
root = tree.getroot()
n = 0
pub_author = []
pub_title = []
pub_dates = []
pub_descr = []
pub_identifiers = []
pub_type = []

class LineCorpus(gensim.corpora.textcorpus.TextCorpus):
    def get_texts(self):
        with open(self.input) as f:
            for l in f:
                yield l.split()

for child in root:
    for key in child:
        for elem in key:
            tmp = []
            if 'metadata' in elem.tag:
                get_items = [key.tag for key in elem.iter()]
                for i in range(0, len(get_items)):
                    line = elem[0][i - 2].text
                    if 'date' in get_items[i]:
                        pub_dates.append(line[:4])
                    if 'creator' in get_items[i]:
                        pub_author.append(line)
                    if 'title' in get_items[i]:
                        pub_title.append(line)
                    if 'description' in get_items[i]:
                        tmp.append(line)
                    if 'identifier' in get_items[i]:
                        if 'https:' in line:
                            pub_identifiers.append(line)
                    if 'type' in get_items[i]:
                        if 'info:' in line:
                            pub_type.append(line)
            pub_descr.append(tmp)

print('------------------------')
print('https://iclc.toplap.org/')
print('------------------------')
print('2015 counts:',  pub_dates.count('2015'), ' -- University of Leeds, UK, 2015')
print('2016 counts:',  pub_dates.count('2016'), ' -- McMaster University, Canada, 2016')
print('2017 counts:',  pub_dates.count('2017'), ' -- Morelia, México, 2017')
print('2018 counts: No ICLI !!!')
print('2019 counts:',  pub_dates.count('2019'), ' -- Madrid, Spain, 2019')
print('2020 counts:',  pub_dates.count('2020'), ' -- Limerick, Ireland, 2020')
sum = pub_dates.count('2015') + pub_dates.count('2016') + pub_dates.count('2017') + pub_dates.count('2019') + pub_dates.count('2020')
print('Total counts =', sum)

n = 0
for i in range(0, len(pub_identifiers)):
    if 'book' not in pub_type[i]:
        r = requests.get(pub_identifiers[i])
        s = str(r.content)
        p = r'href=[\'"]?([^\'" >]+)\.(pdf\?download=1)'
        match = re.search(p, s)
        if match:
            print(match.group(1) + '.pdf')
            result = match.group(1) + '.pdf'
            file = 'https://zenodo.org' + result
            output = urlretrieve(file)
            os.rename(output[0], data_pwd + '/output_' + str(n) + '.pdf')
            n = n + 1

os.putenv("COUNT", n)
os.system("echo $NUM")

# # corpus_descr = LineCorpus(pub_descr)
# corpus_title = LineCorpus(pub_title)

# # model_descr = gensim.models.LdaModel(corpus_descr,
# #                                id2word=corpus_descr.dictionary,
# #                                alpha='auto',
# #                                num_topics=10,
# #                                passes=5)

# model_title = gensim.models.LdaModel(corpus_title,
#                                id2word=corpus_title.dictionary,
#                                alpha='auto',
#                                num_topics=10,
#                                passes=5)

# # model_descr.save('model_descr.lda.result')
# model_title.save('model_title.lda.result')
