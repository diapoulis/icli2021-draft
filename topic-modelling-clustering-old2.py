from __future__ import print_function
import re
import nltk
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
import os
from os import listdir
from os.path import isfile, join
import csv
import re
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import *
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
import gensim
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
import codecs

work_pwd = os.getcwd()
data_pwd = work_pwd + '/data/'
onlyfiles = [f for f in listdir(data_pwd) if isfile(join(data_pwd, f))]
onlyfiles

lst_of_fn = dict()
# ﬀ 64256
# ﬁ 64257
# ﬂ 64258

ff = chr(64256)
fi = chr(64257)
fl = chr(64258)
ffi = chr(64259)

for fn in onlyfiles:
    #file = open(data_pwd + fn, "r")
    with codecs.open(data_pwd + fn, 'r', encoding='utf-8',
                     errors='ignore') as file:
        base = os.path.basename(fn)
        lst_of_fn[base] = []
        lst_of_fn[base].append({ 'voc': [] })
        lst_of_fn[base].append({ 'ref': () })
        cnt = 0
        for line in file:
            cnt = cnt + 1
            # print(line)
            if ('References' in line) or ('REFERENCES' in line):
                lst_of_fn[base][1]['ref'] = (line, cnt)
            elif lst_of_fn[base][1]['ref'] == ():
                word_lst = re.sub("[^\w]", " ",  line).split()
                for word in word_lst:
                    if len(word) > 1:
                        if ff in word:
                            tmp = word.replace(ff, 'ff')
                            lst_of_fn[base][0]['voc'].append(tmp)
                        elif fi in word:
                            tmp = word.replace(fi, 'fi')
                            lst_of_fn[base][0]['voc'].append(tmp)
                        elif fl in word:
                            tmp = word.replace(fl, 'fl')
                            lst_of_fn[base][0]['voc'].append(tmp)
                        elif ffi in word:
                            tmp = word.replace(ffi, 'ffi')
                            lst_of_fn[base][0]['voc'].append(tmp)
                        else:
                            lst_of_fn[base][0]['voc'].append(word)
vocabulary = []
for base, value in lst_of_fn.items():
    for w in lst_of_fn[base][0]['voc']:
        vocabulary.append(w)
print('len(vocabulary):', len(vocabulary))
# Vocabulary per document
docs = []
for base, value in lst_of_fn.items():
    docs.append(lst_of_fn[base][0]['voc'])

print('len(docs):', len(docs), 'len(lst_of_fn.items()):', len(lst_of_fn.items()))
# Stopwords and cleaning
stopwords = []
fn = 'stop-word-list.txt'
file = open(work_pwd + '/' + fn, "r")
for line in file:
    line = line.strip('\n')
    stopwords.append(line)

## Remove stopwords
docs_no_stopwords = []
for doc in docs:
    for word in doc:
        if word in stopwords:
            doc.remove(word)
    docs_no_stopwords.append(doc)

print('len(docs_no_stopwords):', len(docs_no_stopwords))

stemmer = PorterStemmer()
stemmer2 = SnowballStemmer("english")
corpora = []
for d in docs_no_stopwords:
    corpora.append([stemmer2.stem(word) for word in d])

print('len(corpora):', len(corpora))
corpus = []

# for doc in docs_no_stopwords:    # no stemming
for doc in corpora:             # with stemming
    mystr = str()
    mystr = " ".join(doc)
    corpus.append(mystr)

print('len(docs):', len(docs))

vectorizer = TfidfVectorizer(stop_words='english')
doc_matrix = vectorizer.fit_transform(corpus)

print('np.shape(doc_matrix):', np.shape(doc_matrix))
print(doc_matrix[1])
vocab_inverted = {}
for word, column_index in vectorizer.vocabulary_.items():
    vocab_inverted[column_index] = word

# KMeans
clusterer = KMeans(n_clusters=3, verbose=True, n_init=30)

clustered_docs = clusterer.fit_predict(doc_matrix)

## Summarize clusters

def summarize_cluster(center, inv_voc, n):
    # the centroid is a vector; we'll first make a list of weights and their corresponding dimensions
    # e.g. [ (0.345, 0), (1.48, 1), (0.95, 2), ...]
    center_index = [ (x, i) for i, x in enumerate(center) ]

    # we sort this by the weights and select the top n
    topn = sorted(center_index, reverse=True)[:n]

    # we finally map this back to words, using the inverted vocabulary dict that we created above
    return [ inv_voc[i] for _, i in topn ]

# TOPICS Clustering
print('--- KMeans ------')
for i, c in enumerate(clusterer.cluster_centers_):
    print('{}:'.format(i), ' '.join(summarize_cluster(c, vocab_inverted, 10)))
print('---========------')

# [
#     clusterer.cluster_centers_[0],
#     clusterer.cluster_centers_[1],
#     clusterer.cluster_centers_[2],
#     clusterer.cluster_centers_[3],
#     clusterer.cluster_centers_[4],
# ]

# Write corpus to file
with open(work_pwd + '/' + 'icli-corpus.txt', 'w') as f:
    for item in corpus:
        f.write("%s\n" % item)

# LOAD CORPUS
corpus_lda = gensim.corpora.textcorpus.TextCorpus(work_pwd + '/' + 'icli-corpus.txt')
# LDA -- Topic modelling
model = gensim.models.LdaModel(corpus_lda, id2word=corpus_lda.dictionary,
                               alpha='auto',
                               num_topics=4,
                               passes=5)

print([model.show_topic(0), model.show_topic(1), model.show_topic(2), model.show_topic(3)])
# models
for topic_id in range(model.num_topics):
    topk = model.show_topic(topic_id, 10)
    topk_words = [ w for w, _ in topk ]

    print('{}: {}'.format(topic_id, ' '.join(topk_words)))

# most
words = []
how_many = 10
frequencies = np.empty((4, how_many))

for i in range(0, 4):
    words.append([])
    for j in range(0, len(model.show_topic(0, how_many))):
        words[i].append(model.show_topic(i, how_many)[j][0])
        frequencies[i][j] = model.show_topic(i, how_many)[j][1]

print(words, frequencies)

# PLOT
for i in range(0, 4):
    objects = words[i]
    y_pos = np.arange(len(objects))
    performance = frequencies[i]
    plt.subplot(eval('22' + f'{i+1}'))
    plt.barh(y_pos, performance, align='center', alpha=0.5)
    plt.yticks(y_pos, objects)
    plt.xlabel('Words frequency')
    plt.title(f'Topic {i}')

plt.tight_layout()
plt.savefig(work_pwd + '/' + 'plot.png')
