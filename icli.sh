curl https://zenodo.org/oai2d\?verb=ListRecords\&set=user-iclc\&metadataPrefix=oai_dc >icli.xml
mkdir data
# download pdf files from Zenodo (store them in data dir)
# python3 retrieve-icli-proceedings.py
# HOW TO COUNT FILES?  -  need to export a bash variable from Python
python3 retrieve-icli-proceedings.py
NUM=$(ls $HOME/data | wc -l)
# convert pdf files to txt
for i in {1..86} # $(seq 1 $NUM) - 86 with 2021 proceedings
do
    pdftotext $HOME/icli2021-draft/data/output_$i.pdf
done
# delete pdf files from data folder
mkdir $HOME/icli2021-draft/pdfs
mv $HOME/icli2021-draft/data/output*.pdf $HOME/icli2021-draft/pdfs
python3 topic-modelling-clustering.py
